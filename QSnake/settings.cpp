#include "settings.h"

Settings& Settings::getInstance()
{
    static Settings instance;
    return instance;
}

// now hardcoded for simplicity - can be changed later
Settings::Settings()
    : timerPeriod(250), boardWidth(25), boardHeight(25),
      colorBackground(QColor(230, 230, 230)), colorSnake(QColor(0, 200, 0)), colorFood(QColor(255, 0, 0))
{

}
