#ifndef ARRAY2D_H
#define ARRAY2D_H

#include <QtGlobal>
#include <vector>
#include <cstddef>

template <typename T>
struct Array2D
{
    Array2D(int number_of_rows, int number_of_columns, T &&value)
        : rows(number_of_rows), cols(number_of_columns), vector(rows*cols, value)
    {}

    T& at(std::pair<int, int> pos)
    {
        Q_ASSERT(isCorrect(pos));
        return vector[cols*pos.second + pos.first];
    }

    bool isCorrect(std::pair<int, int> pos)
    {
        return pos.first < cols && pos.second < rows && pos.first >= 0 && pos.second >= 0;
    }

    const int rows, cols;
    std::vector<T> vector;
};

#endif // ARRAY2D_H
