#ifndef SETTINGS_H
#define SETTINGS_H

#include <QColor>

class Settings
{
public:
    static Settings &getInstance();
    int getTimerPeriod() const { return timerPeriod; }
    int getBoardWidth() const { return boardWidth; }
    int getBoardHeight() const { return boardHeight; }
    QColor getColorBackground() const { return colorBackground; }
    QColor getColorSnake() const { return colorSnake; }
    QColor getColorFood() const { return colorFood; }

private:
    int timerPeriod; // in milliseconds
    int boardWidth; // number of fields (horizontal)
    int boardHeight; // number of fields (vertical)
    QColor colorBackground;
    QColor colorSnake;
    QColor colorFood;

    Settings();
    Settings(const Settings&) = delete;
};

#endif // SETTINGS_H
