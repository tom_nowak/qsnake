#include "board.h"
#include <ctime>
#include <random>

Board::Board(unsigned width, unsigned height)
    : fieldsArray(width, height, FREE), snake(width/2, height/2), freeFields(width*height - 3)
{
    srand(time(0));
    fieldsArray.at(snake.getHeadPosition()) = SNAKE;
    fieldsArray.at(snake.getTailPosition()) = SNAKE;
    spawnFood();
    Q_ASSERT(getSnakeLength() == 2);
}

size_t Board::getSnakeLength()
{
    return fieldsArray.rows * fieldsArray.cols - freeFields - 1;
}

void Board::setNextDirectionChange(Snake::DirectionChange dir)
{
    nextDirectionChange = dir;
}

bool Board::nextMove()
{
    snake.moveHead(nextDirectionChange);
    nextDirectionChange = Snake::DirectionChange::NONE;
    auto head = snake.getHeadPosition();
    if(!fieldsArray.isCorrect(head))
        return false;
    if(fieldsArray.at(head) == SNAKE)
        return false;
    if(fieldsArray.at(head) == FREE)
    {
        fieldsArray.at(snake.getTailPosition()) = FREE;
        snake.moveTail();
    }
    else
    {
        --freeFields;
        spawnFood();
    }
    fieldsArray.at(head) = SNAKE;
    return true;
}

Board::FieldType Board::getFieldAt(std::pair<int, int> pos)
{
    return fieldsArray.at(pos);
}

int Board::getWidth()
{
    return fieldsArray.cols;
}

int Board::getHeight()
{
    return fieldsArray.rows;
}

void Board::spawnFood()
{
    int field_number = rand() % freeFields;
    for(FieldType &field : fieldsArray.vector)
    {
        if(field == FREE)
        {
            if(field_number-- == 0)
            {
                field = FOOD;
                break;
            }
        }
    }
}
