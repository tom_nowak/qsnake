#include "snake.h"
#include <cmath>
#include <cstdio>

Snake::Snake(int x_start, int y_start)
{
    snakeSegmentsList.emplace_back(x_start, y_start);
    snakeSegmentsList.emplace_back(x_start, y_start + 1); // one below the snake head
    Q_ASSERT(snakeSegmentsList.front().getDirection(snakeSegmentsList.back()) == UP);
    Q_ASSERT(snakeSegmentsList.front().isNeighbour(snakeSegmentsList.back()));
    Q_ASSERT(snakeSegmentsList.back().isNeighbour(snakeSegmentsList.front()));
}

std::pair<int, int> Snake::getHeadPosition()
{
    return std::pair<int, int>(snakeSegmentsList.front());
}

std::pair<int, int> Snake::getTailPosition()
{
    return std::pair<int, int>(snakeSegmentsList.back());
}

void Snake::moveHead(DirectionChange direction_change)
{
    Direction dir = getDirectionAfterChange(direction_change);
    if(direction_change)
        snakeSegmentsList.push_front(snakeSegmentsList.front()); // after changing direction - create new segment
    snakeSegmentsList.front().move(dir);
}

void Snake::moveTail()
{
    auto last_segment = snakeSegmentsList.end();
    Q_ASSERT(last_segment != snakeSegmentsList.begin());
    --last_segment;
    auto before_last_segment = last_segment;
    --before_last_segment;
    if(before_last_segment->isNeighbour(*last_segment))
    {
        snakeSegmentsList.erase(last_segment);
        return;
    }
    last_segment->move(before_last_segment->getDirection(*last_segment));
}

Snake::Direction Snake::getDirectionAfterChange(DirectionChange direction_change)
{
    auto first_segment = snakeSegmentsList.begin();
    Q_ASSERT(first_segment != snakeSegmentsList.end());
    auto second_segment = first_segment;
    ++second_segment;
    Q_ASSERT(second_segment != snakeSegmentsList.end());
    // below: +4 in case of change: UP -> LEFT (direction change -1) because % is undefined for negative numbers
    int tmp = static_cast<int>(first_segment->getDirection(*second_segment)) + 4;
    return static_cast<Snake::Direction>((tmp + direction_change) % 4);
}

bool Snake::SnakeSegment::isNeighbour(Snake::SnakeSegment &other)
{
    if(first == other.first)
        return std::abs(second - other.second) == 1;
    return (std::abs(first - other.first) == 1) && (second == other.second);
}

Snake::Direction Snake::SnakeSegment::getDirection(Snake::SnakeSegment &other)
{
    Q_ASSERT(second != other.second || first != other.first);
    Q_ASSERT(second == other.second || first == other.first);
    if(first == other.first)
    {
        if(second < other.second)
            return UP;
        return DOWN;
    }
    if(first < other.first)
        return LEFT;
    return RIGHT;
}

void Snake::SnakeSegment::move(Snake::Direction dir)
{
    switch(dir)
    {
        case UP:
            --second;
            break;
        case RIGHT:
            ++first;
            break;
        case DOWN:
            ++second;
            break;
        case LEFT:
            --first;
    }
}
