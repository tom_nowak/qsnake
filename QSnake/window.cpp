#include "window.h"
#include "settings.h"
#include <QTimer>
#include <QPainter>
#include <QDebug>
#include <QKeyEvent>
#include <QMessageBox>
#include <QPalette>
#include <QApplication>
#include <QDesktopWidget>

Window::Window(QWidget *parent)
    : QWidget(parent), board(Settings::getInstance().getBoardWidth(),
      Settings::getInstance().getBoardHeight()), paused(false)
{
    setUpSize();
    setUpBackgroundColor();
    setUpTimer();
}

Window::~Window()
{

}

void Window::timeoutHandler()
{
    qDebug() << "Window::timeoutHandler()";
    if(paused)
        return;
    if(!board.nextMove())
        finish();
    update();
}

void Window::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
        case Qt::Key_Escape:
            qDebug() << "key escape";
            paused = true;
            finish();
            break;
        case Qt::Key_Space:
            qDebug() << "key space";
            paused = !paused;
            break;
        case Qt::Key_Left:
            qDebug() << "key left";
            if(!paused)
                board.setNextDirectionChange(Snake::DirectionChange::TURN_LEFT);
            break;
        case Qt::Key_Right:
            qDebug() << "key right";
            if(!paused)
                board.setNextDirectionChange(Snake::DirectionChange::TURN_RIGHT);
    }
}

void Window::paintEvent(QPaintEvent *event)
{
    qDebug() << "Window::paintEvent()";
    float total_field_width = static_cast<float>(width()) / board.getWidth();
    float total_field_height = static_cast<float>(height()) / board.getHeight();
    float margin_x = total_field_width / 20.0;
    float margin_y = total_field_height / 20.0;
    float fill_field_width = 0.9 * total_field_width;
    float fill_field_height = 0.9 * total_field_height;
    QPainter qp(this);
    for(int y = 0; y < board.getHeight(); ++y)
    {
        float top = static_cast<float>(y) * total_field_height + margin_y;
        for(int x = 0; x < board.getWidth(); ++x)
        {
            float left = static_cast<float>(x) * total_field_width + margin_x;
            QRectF field(left, top, fill_field_width, fill_field_height);
            switch(board.getFieldAt(std::pair<int, int>(x, y)))
            {
                case Board::FieldType::FREE:
                    break;
                case Board::FieldType::SNAKE:
                    qp.fillRect(field, Settings::getInstance().getColorSnake());
                    break;
                case Board::FieldType::FOOD:
                    qp.fillRect(field, Settings::getInstance().getColorFood());
            }
        }
    }
    QWidget::paintEvent(event);
}

void Window::finish()
{
    QMessageBox mb(this);
    QString text = QString("Game over! Snake length: %1").arg(board.getSnakeLength());
    mb.setWindowTitle("GAME OVER");
    mb.setText(text);
    mb.exec();
    close();
}

void Window::setUpBackgroundColor()
{
    QPalette pal = palette();
    pal.setColor(QPalette::Background, Settings::getInstance().getColorBackground());
    setAutoFillBackground(true);
    setPalette(pal);
}

void Window::setUpTimer()
{
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timeoutHandler()));
    timer->start(Settings::getInstance().getTimerPeriod());
}

void Window::setUpSize()
{
    QRect rec = QApplication::desktop()->screenGeometry();
    int height = rec.height();
    int width = rec.width();
    int size = height < width ? height : width;
    setGeometry(0, 0, size, size);
}
