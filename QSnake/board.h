#ifndef BOARD_H
#define BOARD_H

#include <QtGlobal>
#include "array2d.h"
#include "snake.h"

class Board
{
public:
    enum FieldType : char
    {
        FREE,
        SNAKE,
        FOOD
    };

    Board(unsigned width, unsigned height);
    size_t getSnakeLength();
    void setNextDirectionChange(Snake::DirectionChange dir);
    bool nextMove(); // returns true if snake has not crashed after next move
    FieldType getFieldAt(std::pair<int, int> pos);
    int getWidth();
    int getHeight();

private:
    Array2D<FieldType> fieldsArray;
    Snake snake;
    Snake::DirectionChange nextDirectionChange;
    int freeFields;

    void spawnFood();
};

#endif // BOARD_H
