#ifndef SNAKE_H
#define SNAKE_H

#include <QtGlobal>
#include <list>

class Snake
{
public:
    enum Direction : int
    {
        UP = 0,
        RIGHT = 1,
        DOWN = 2,
        LEFT = 3
    };

    enum DirectionChange : int
    {
        TURN_LEFT = -1,
        NONE = 0,
        TURN_RIGHT = 1
    };

    Snake(int x_start, int y_start);
    std::pair<int, int> getHeadPosition();
    std::pair<int, int> getTailPosition();
    void moveHead(DirectionChange direction_change);
    void moveTail();

private:
    struct SnakeSegment : public std::pair<int, int>
    {
        SnakeSegment(int x, int y)
            : std::pair<int, int>(x,y)
        {}
        bool isNeighbour(SnakeSegment &other);
        Direction getDirection(SnakeSegment &other);
        void move(Direction dir);
    };

    std::list<SnakeSegment> snakeSegmentsList;

    Direction getDirectionAfterChange(DirectionChange direction_change);
};

#endif // SNAKE_H
