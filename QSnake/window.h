#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include "board.h"

class Window : public QWidget
{
    Q_OBJECT

public:
    Window(QWidget *parent = 0);
    ~Window();

protected slots:
    void timeoutHandler();
    virtual void keyPressEvent(QKeyEvent *event);

protected:
    Board board;
    bool paused;

    virtual void paintEvent(QPaintEvent *event);
    void finish();
    void setUpBackgroundColor();
    void setUpTimer();
    void setUpSize();
};

#endif // WINDOW_H
